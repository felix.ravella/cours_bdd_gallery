import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ArtistModule } from './artist/artist.module';
import { PodiumModule } from './podium/podium.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: '127.0.0.1',
      port: 3306,
      username: 'galleryDev',
      password: 'dDG-wM_Bdgp3jMC^',
      database: 'gallery',
      autoLoadEntities: true,
      synchronize: true
    }),
    ArtistModule,
    PodiumModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
