import { Module } from '@nestjs/common';
import { PodiumController } from './podium.controller';

@Module({
  controllers: [PodiumController],
  providers: []
})
export class PodiumModule {}
