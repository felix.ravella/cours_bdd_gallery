import { Controller, Get } from '@nestjs/common';
import { InjectDataSource } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';


/* Work In progress */

@Controller('podium')
export class PodiumController {
  constructor(@InjectDataSource() private bdd: DataSource) {}

  @Get()
  async findAll(){
    try {
      const artists = await this.bdd.query(
        `SELECT * FROM Artist;`
      );
      return artists
    } catch (err) {
      console.log(err);
      return (err)
    }
  }
}
