import { Body, Controller, Delete, Get, Injectable, Param, Patch, Post } from '@nestjs/common';
import { InjectDataSource } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';

@Controller('artist')
@Injectable()
export class ArtistController {
  constructor(@InjectDataSource() private bdd: DataSource) {}

  @Post()
  async create(@Body() body){
    const pseudo = body.pseudo ? `"${body.pseudo}"`: null;
    try {
      const artist = await this.bdd.query(
        "INSERT INTO Artist (firstName, lastName, pseudo) values (?, ?, ?);"
        [
          body.firstName,
          body.lastName,
          pseudo
        ]
      );
      return "Succesfully created Artist"
    } catch (err) {
      console.log(err);
      return (err)
    }
  }

  @Get()
  findAll(){
    try {
      const artists = this.bdd.query(
        `SELECT * FROM Artist;`
      );
      return artists
    } catch (err) {
      console.log(err);
      return (err)
    }
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    try {
      const result = await this.bdd.query(
        "SELECT * FROM Artist WHERE id=?;", [id]
      );
      let artist = result[0]

      let paintings = await this.bdd.query(
        "SELECT * FROM Painting WHERE artistId=?;", [id]
      );
      artist["paintings"] = paintings;
      console.log(artist)
      return artist;
    } catch (err) {
      console.log(err);
      return;
    }
  }

  @Patch(':id')
  async update(@Param('id') id: string, @Body() body) {

    try {
      const parseBody = JSON.parse(JSON.stringify(body))
      const result = await this.bdd
      .createQueryBuilder()
      .update('Artist')
      .set(parseBody)
      .where(`id = ${id}`)
      .execute()
      /*
      const artist = await this.bdd.query(
        `UPDATE Artist SET firstName = "${body.firstName}", lastName = "${body.lastName}", pseudo = "${body.pseudo}" WHERE id = ${id} ;`
      );
      */
    } catch (err) {
      console.log(err);
      return;
    }
  }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    try {
      const result = await this.bdd.query(
        "DELETE FROM Artist WHERE id=?;", [id]
      );
      return result;
    } catch (err) {
      console.log(err);
      return;
    }
  }


  


}
